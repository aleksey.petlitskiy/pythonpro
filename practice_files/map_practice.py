# filter
numbers = [1, 3, 2, 5, 7, 8]
even_numbers = list(filter(lambda number: number % 2 == 0, numbers))
print(even_numbers)

# list comprehension
numbers = [1, 3, 2, 5, 7, 8]
even_numbers = [number for number in numbers if number % 2 == 0]
print(even_numbers)

# python
numbers = [1, 3, 2, 5, 7, 8]
even_numbers = []
for number in numbers:
    if number % 2 == 0:
        even_numbers.append(number)
print(even_numbers)


def get_digit(number):
    return 0 if number <= 0 else 1


numbers = [1, -2, -4, 2, 5, -9]
# digits = list(map(lambda number: 0 if number <= 0 else 1, numbers))
# print(digits)
digits1 = [get_digit(number) for number in numbers]
print(digits1)


numbers = [1, -2, -4, 2, 5, -9]
digits2 = []
for number in numbers:
    digits2.append(1 if number <= 0 else 0)

    if number <= 0:
        digits2.append(1)
    else:
        digits2.append(0)
print(digits2)

# --------------------

products = [{
    "price": 20,
    "is_discount": False,
},
    {
        "price": 50,
        "is_discount": True,
    },
    {
        "price": 100,
        "is_discount": False,
    }]

new_product = list(map(lambda product: {"is_discount": False, "price": product["price"]+10},

                  filter(lambda product: product["is_discount"] == False, products)))

print(new_product)
