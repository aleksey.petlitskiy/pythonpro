#  1. Реализовать функцию, которая принимает строку и удаляет символы на четных позициях. Например, abcdef --> bdf
#
#  подумай, как ты можешь проитерироваться по индексам строки с помощью функции range()
# и потом просто проверяешь что если индекс % 2 == 0 то по этому индексу берешь из строки символ и прибавляешь его к какому-то резалтСтринг


def del_symbol_on_even_position(string):
    original_string = list(string)
    result_string = []
    for pos in range(0, len(original_string)):
        if pos % 2 == 0:
            result_string.append(original_string[pos])
    return ''.join(result_string)


print(del_symbol_on_even_position("abcdef"))


# --------------------------------------


def del_symbol_on_even_position(string):
    result_string = ""
    for pos in range(0, len(string)):
        if pos % 2 == 0:
            result_string += string[pos]
    return result_string


print(del_symbol_on_even_position("abcdef"))

# 2. Реализовать функцию, которая принимает число
# и возвращает его факториал. Например, 5 --> 1×2×3×4×5.


def get_factorial(integer):
    num = 2
    result = 1
    while num <= integer:
        result *= num
        num += 1
    return result


print(get_factorial(5))


def get_factorial(integer):
    result = 1
    for num in range(2, integer+1):
        result *= num
    return result


print(get_factorial(5))


result = 1


def get_factorial(integer):

  if integer == 1:
    return 1
  return get_factorial(integer - 1) * integer


print(get_factorial(5))


# 3. Реализовать функцию, которая принимает число, сортирует цифры в обратном порядке и возвращает новое число. Например, 142 --> 421


def sorting_numbers(integer):
    result = sorted(str(integer))
    return ''.join(result[::-1])


print(int(sorting_numbers(131321231435646457)))
