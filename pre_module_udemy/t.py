# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # a = {}
# # # # dfg = []
# # # # for key, value in abc.items():
# # # #     dfg.append({value: key})
# # # # for i in dfg:
# # # #     a.update(i)
# # # #
# # # # print(a)
# # # #
# # # # # ----------------------
# # # #
# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # a = {}
# # # # dfg = []
# # # # for items in abc.items():
# # # #     dfg.append(items)
# # # # s = []
# # # # for it in dfg:
# # # #     s.append(it[::-1])
# # # #
# # # # print(dict(s))
# # # # # ----------------------
# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # cba = {value: key for key, value in abc.items()}
# # # # print(cba)
# # # #
# # # #
# # # # dct = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
# # # # sorted_dct_ascending = sorted(dct.items(), key=lambda item: item[1])[-1][-1]
# # # # print(sorted_dct_ascending)
# # #
# # # # def generator_1():
# # # #     yield 1
# # # #     yield 2
# # # #     yield 3
# # # #
# # # #
# # # # gen_obj = generator_1()
# # # # print(type(gen_obj))
# # # # for value in gen_obj:
# # # #     print(value)
# # #
# # # """
# # # окей, задачка с тех интервью
# # # 1. есть строка "nvustruekrnguwutg"
# # # нам нужно посчитать count() каждого элемента и выдать макс элемент
# # # пример
# # # у нас строка "aabab"
# # # count a = 3
# # # count b = 2
# # # соответственно вывод должен быть a 3
# # # """
# # # import time
# # #
# # # # letters = 'nvusteuuruek'
# # # # new_dict = {}
# # # # for letter in letters:
# # # #     new_dict.update({letter: letters.count(letter)})
# # # # print({key: value for key, value in new_dict.items() if value == sorted(new_dict.values())[-1]})
# # # # "сделать беез циклов с лямбдой"--------!!!!!!!!!!!!
# # #
# # # # for letter in letters:
# # # #     new_dict.update({letter: letters.count(letter)})
# # # # for key, value in new_dict.items():
# # # #     if value == sorted(new_dict.values())[-1]:
# # # #         new_dict_1 = {key, value}
# # # #         print(new_dict_1)
# # #
# # # """
# # # предположим у нас есть словарь, в котором много значений, задача убрать одинаковые value из словаря,
# # # оставляя только ключ для первого вхождения
# # # пример:
# # # {a: 1
# # #     b:2
# # #     c: 1}
# # # =>
# # # {a:1
# # #     b:2}
# # # """
# # # # new_dict = {}
# # # # my_dict_1 = {'a': 1, 'b': 2, 'c': 1}
# # # # for key, value in my_dict_1.items():
# # # #     if value not in new_dict.values():
# # # #         new_dict.update({key: value})
# # # # print(new_dict)
# # #
# # #
# # # # import asyncio
# # # #
# # # #
# # # # async def foo():
# # # #     print("Start foo")
# # # #     await asyncio.sleep(2)
# # # #     print("End foo")
# # # #
# # # #
# # # # async def bar():
# # # #     print("Start bar")
# # # #     await asyncio.sleep(1)
# # # #     print("End bar")
# # # #
# # # #
# # # # # Создаем событийный цикл и запускаем корутины
# # # # loop = asyncio.get_event_loop()
# # # # loop.run_until_complete(asyncio.gather(foo(), bar()))
# # #
# # # '''
# # # По кодингу
# # # 1. Прочитать о декораторах
# # # 2. Написать функцию декоратор, которая будет выводить "before" перед кодом осн функции и "after" после
# # # 3. Написать функцию- декоратор, для подсчета времени работы других функций
# # # '''
# # #
# # # # def execution_time_count(funct):
# # # #     def wrapper(*args, **kwargs):
# # # #         start_time = time.perf_counter()
# # # #         funct(*args, **kwargs)
# # # #         end_time = time.perf_counter()
# # # #         print(end_time - start_time)
# # # #         return funct
# # # #     return wrapper
# # # #
# # # #
# # # # def decorator_func(func):
# # # #     def wrapper(*args, **kwargs):
# # # #         print('before')
# # # #         func(*args, **kwargs)
# # # #         print('after')
# # # #         return func
# # # #     return wrapper
# # # #
# # # #
# # # # @execution_time_count
# # # # @decorator_func
# # # # def print_text(text):
# # # #     time.sleep(5)
# # # #     print(f'{text}')
# # # #
# # # #
# # # # print_text("some text in the middle")
# # #
# # # massive = []
# # #
# # #
# # # def split_string(text1):
# # #     for letter in text1:
# # #         massive.append(letter)
# # #         if letter == letter.upper():
# # #             break
# # #         print(massive)
# # #
# # #
# # # split_string('someText')
# # #
# # # text = 'Hello Python!'
# # # print(text[:2])
# # # print(text[-13:-11])
# # # b = []
# # # c = ''
# # # for i in text[:2]:
# # #     b.append(i)
# # #     c = "".join(b)
# # # print(c)
# # #
# # # tezt = 'Hello Python!'
# # # print(tezt[-7] + 'a' + tezt[-5:-3])
# # #
# # # tezt = 'ello ython!'
# # # print(tezt.upper())
# # # print(tezt.capitalize())
# # #
# # # print('''
# # #  {0:15.4f} {1:15.4f} {2:15.4f} {3:15.4f}
# # #  {4:15.4f} {5:15.4f} {6:15.4f} {7:15.4f}
# # # '''.format(34.2344, 1234.23, 1.45778, 345.232352,
# # #            1.45778, 345.232352, 34.2344, 1234.23))
# # #
# # # letters_set = set('Создайте множество при помощи функции set() '
# # #                   'из текста, чтобы получить уникальные символы, '
# # #                   'содержащиеся в тексте.')
# # # print(letters_set)
# # #
# # # sum_of_even = 0
# # # for num in range(10, 30):
# # #     if num % 2 == 0:
# # #         sum_of_even = sum_of_even + num
# # #         print(sum_of_even)
# # #
# # # # hmt = int(input("enter the number: "))
# # # # for i in range(hmt):
# # # #     print("helllllooo")
# # #
# # # '''Из исходного списка greetings = ['hello', 'hi', 'hey', 'hola'] создайте новый список
# # # содержащий вторую букву из каждой строки исходного списка. Выведите новый список на печать.
# # # Решите задание двумя способами - при помощи List Comprehension и без него.'''
# # #
# # # greetings = ['hello', 'hi', 'hey', 'hola']
# # # letter_list = [i[1] for i in greetings]
# # # print(letter_list)
# # #
# # # even_list_1 = []
# # # digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# # # for num in digits:
# # #     if num % 2 != 0:
# # #         even_list_1.append(num)
# # # print(even_list_1)
# # #
# # # even_list_1 = [num for num in digits if num % 2 != 0]
# # # print(even_list_1)
# # #
# # # i = 0
# # # while i < 5:
# # #     i += 1
# # #     ll = [a for a in range(i)]
# # #     print(ll)
# # #
# # #
# # # i = 1
# # # ll = []
# # # while i < 5:
# # #     i += 1
# # #     for a in range(i):
# # #         ll.append('\U0001f600')
# # #         print(ll)
# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # a = {}
# # # # dfg = []
# # # # for key, value in abc.items():
# # # #     dfg.append({value: key})
# # # # for i in dfg:
# # # #     a.update(i)
# # # #
# # # # print(a)
# # # #
# # # # # ----------------------
# # # #
# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # a = {}
# # # # dfg = []
# # # # for items in abc.items():
# # # #     dfg.append(items)
# # # # s = []
# # # # for it in dfg:
# # # #     s.append(it[::-1])
# # # #
# # # # print(dict(s))
# # # # # ----------------------
# # # # abc = {1: 2, 3: 4, 5: 6}
# # # #
# # # # cba = {value: key for key, value in abc.items()}
# # # # print(cba)
# # # #
# # # #
# # # # dct = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
# # # # sorted_dct_ascending = sorted(dct.items(), key=lambda item: item[1])[-1][-1]
# # # # print(sorted_dct_ascending)
# # #
# # # # def generator_1():
# # # #     yield 1
# # # #     yield 2
# # # #     yield 3
# # # #
# # # #
# # # # gen_obj = generator_1()
# # # # print(type(gen_obj))
# # # # for value in gen_obj:
# # # #     print(value)
# # #
# # # """
# # # окей, задачка с тех интервью
# # # 1. есть строка "nvustruekrnguwutg"
# # # нам нужно посчитать count() каждого элемента и выдать макс элемент
# # # пример
# # # у нас строка "aabab"
# # # count a = 3
# # # count b = 2
# # # соответственно вывод должен быть a 3
# # # """
# # # import time
# # #
# # # # letters = 'nvusteuuruek'
# # # # new_dict = {}
# # # # for letter in letters:
# # # #     new_dict.update({letter: letters.count(letter)})
# # # # print({key: value for key, value in new_dict.items() if value == sorted(new_dict.values())[-1]})
# # # # "сделать беез циклов с лямбдой"--------!!!!!!!!!!!!
# # #
# # # # for letter in letters:
# # # #     new_dict.update({letter: letters.count(letter)})
# # # # for key, value in new_dict.items():
# # # #     if value == sorted(new_dict.values())[-1]:
# # # #         new_dict_1 = {key, value}
# # # #         print(new_dict_1)
# # #
# # # """
# # # предположим у нас есть словарь, в котором много значений, задача убрать одинаковые value из словаря,
# # # оставляя только ключ для первого вхождения
# # # пример:
# # # {a: 1
# # #     b:2
# # #     c: 1}
# # # =>
# # # {a:1
# # #     b:2}
# # # """
# # # # new_dict = {}
# # # # my_dict_1 = {'a': 1, 'b': 2, 'c': 1}
# # # # for key, value in my_dict_1.items():
# # # #     if value not in new_dict.values():
# # # #         new_dict.update({key: value})
# # # # print(new_dict)
# # #
# # #
# # # # import asyncio
# # # #
# # # #
# # # # async def foo():
# # # #     print("Start foo")
# # # #     await asyncio.sleep(2)
# # # #     print("End foo")
# # # #
# # # #
# # # # async def bar():
# # # #     print("Start bar")
# # # #     await asyncio.sleep(1)
# # # #     print("End bar")
# # # #
# # # #
# # # # # Создаем событийный цикл и запускаем корутины
# # # # loop = asyncio.get_event_loop()
# # # # loop.run_until_complete(asyncio.gather(foo(), bar()))
# # #
# # # '''
# # # По кодингу
# # # 1. Прочитать о декораторах
# # # 2. Написать функцию декоратор, которая будет выводить "before" перед кодом осн функции и "after" после
# # # 3. Написать функцию- декоратор, для подсчета времени работы других функций
# # # '''
# # #
# # # # def execution_time_count(funct):
# # # #     def wrapper(*args, **kwargs):
# # # #         start_time = time.perf_counter()
# # # #         funct(*args, **kwargs)
# # # #         end_time = time.perf_counter()
# # # #         print(end_time - start_time)
# # # #         return funct
# # # #     return wrapper
# # # #
# # # #
# # # # def decorator_func(func):
# # # #     def wrapper(*args, **kwargs):
# # # #         print('before')
# # # #         func(*args, **kwargs)
# # # #         print('after')
# # # #         return func
# # # #     return wrapper
# # # #
# # # #
# # # # @execution_time_count
# # # # @decorator_func
# # # # def print_text(text):
# # # #     time.sleep(5)
# # # #     print(f'{text}')
# # # #
# # # #
# # # # print_text("some text in the middle")
# # #
# # # massive = []
# # #
# # #
# # # def split_string(text1):
# # #     for letter in text1:
# # #         massive.append(letter)
# # #         if letter == letter.upper():
# # #             break
# # #         print(massive)
# # #
# # #
# # # split_string('someText')
# # #
# # # text = 'Hello Python!'
# # # print(text[:2])
# # # print(text[-13:-11])
# # # b = []
# # # c = ''
# # # for i in text[:2]:
# # #     b.append(i)
# # #     c = "".join(b)
# # # print(c)
# # #
# # # tezt = 'Hello Python!'
# # # print(tezt[-7] + 'a' + tezt[-5:-3])
# # #
# # # tezt = 'ello ython!'
# # # print(tezt.upper())
# # # print(tezt.capitalize())
# # #
# # # print('''
# # #  {0:15.4f} {1:15.4f} {2:15.4f} {3:15.4f}
# # #  {4:15.4f} {5:15.4f} {6:15.4f} {7:15.4f}
# # # '''.format(34.2344, 1234.23, 1.45778, 345.232352,
# # #            1.45778, 345.232352, 34.2344, 1234.23))
# # #
# # # letters_set = set('Создайте множество при помощи функции set() '
# # #                   'из текста, чтобы получить уникальные символы, '
# # #                   'содержащиеся в тексте.')
# # # print(letters_set)
# # #
# # # sum_of_even = 0
# # # for num in range(10, 30):
# # #     if num % 2 == 0:
# # #         sum_of_even = sum_of_even + num
# # #         print(sum_of_even)
# # #
# # # # hmt = int(input("enter the number: "))
# # # # for i in range(hmt):
# # # #     print("helllllooo")
# # #
# # # '''Из исходного списка greetings = ['hello', 'hi', 'hey', 'hola'] создайте новый список
# # # содержащий вторую букву из каждой строки исходного списка. Выведите новый список на печать.
# # # Решите задание двумя способами - при помощи List Comprehension и без него.'''
# # #
# # # greetings = ['hello', 'hi', 'hey', 'hola']
# # # letter_list = [i[1] for i in greetings]
# # # print(letter_list)
# # #
# # # even_list_1 = []
# # # digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# # # for num in digits:
# # #     if num % 2 != 0:
# # #         even_list_1.append(num)
# # # print(even_list_1)
# # #
# # # even_list_1 = [num for num in digits if num % 2 != 0]
# # # print(even_list_1)
# # #
# # i = 0
# # while i < 10:
# #     i += 1
# #     ll = ['\U0001f600' for a in range(i)]
# #     print(''.join(ll))
# #
# # for nun in range(10):
# #     count = 0
# #     emoticons = ''
# #     while count <= nun:
# #         emoticons += '\U0001f600'
# #         count += 1
# #     print(emoticons)
# #
# # for q in range(1, 11):
# #     print('\U0001f600' * q)
# #
# #
# # def ho(text):
# #     return "hello" in text.lower()
# #
# #
# # print(ho('some text Hello'))
# #
# #
# # def is_cat_here(*args):
# #     return 'cat' in args.__str__().lower()
# #
# #
# # print(is_cat_here("Cat", 'dof', 'pod'))
#
#
# # def is_item_here(item, **args):
# #     return item in args
# #
# #
# # print(is_item_here("cat", cat='CAT',  d='dof', b='pod'))
#
# '''
# Создайте функцию your_favorite_color() с позиционным параметром my_color и параметрами **kwargs,
#  которая будет выводить на экран 'My favorite color is (значение my_color), what is your favorite color?',
#  если в параметрах kwargs нет ключа color,
# и 'My favorite color is (значение my_color), but (значение по ключу color) is also pretty good!',
#  если в параметрах kwargs ключ color присутствует
# '''
# import random
# #
# #
# # def your_favorite_color(my_color, **kwargs):
# #     print(f'My favourite colour is {my_color}, what is your favorite color?') \
# #         if 'color' not in kwargs else \
# #         print(f'My favorite color is {my_color}, but {kwargs["color"]} is also pretty good!')
# #
# #
# # your_favorite_color('green', color='grey')
#
# # def your_favorite_color(my_color, **kwargs):
# #     if 'color' in kwargs:
# #         print('My favorite color is ' + str(my_color) +
# #               ', but ' + kwargs['color'] + ' is also pretty good!')
# #     else:
# #         print('My favorite color is ' + str(my_color) +
# #               ', what is your favorite color?')
# #
# #
# # your_favorite_color('green', color="Grey")
#
#
# # num_list = [1, 2, 3, 4, 5, 6]
# #
# # print(list(map(lambda a: a * a, num_list)))
# #
# # print(list(filter(lambda z: z % 2 == 0, num_list)))
#
# '''
# Создайте класс BlogPost с атрибутами user_name, text, number_of_likes.
# Создайте два объекта этого класса.
# После создания измените атрибут number_of_likes одного из объектов.
# Распечатайте атрибут number_of_likes каждого из объектов
# '''
#
#
# # class BlogPost:
# #     def __init__(self, user_name, text, number_of_likes):
# #         self.user_name = user_name
# #         self.text = text
# #         self.number_of_likes = number_of_likes
# #
# #
# # sa = BlogPost(user_name='Amba', text='xome text', number_of_likes=0)
# # ua = BlogPost('Banana', 'Boom', 0)
# #
# # sa.number_of_likes = 999
# #
# # print(sa.number_of_likes, '\n', ua.number_of_likes)
#
# '''
# Создайте класс BankAccount с атрибутами
# client_id, client_first_name, client_last_name, balance и методами add() и withdraw(),
# при помощи которых можно пополнять счет и выводить средства со счета соответственно.
# Атрибут balance должен инициализироваться по умолчанию значением 0.0 и меняться при срабатывании методов add() и withdraw().
# '''
#
#
# class BankAccount:
#     def __init__(self, client_id, client_first_name, client_last_name):
#         self.client_id = client_id
#         self.client_first_name = client_first_name
#         self.client_last_name = client_last_name
#         self.balance = 0.0
#
#     def add(self, balance_income):
#         self.balance += balance_income
#         return self.balance
#
#     def withdraw(self, balance_withdraw):
#         self.balance -= balance_withdraw
#         return self.balance
#
#
# transaction = BankAccount(random.Random, 'Oleksii', 'Petlytskyi')
#
# transaction.add(10)
# # transaction.withdraw(5)
# transaction.add(1)
# print(transaction.balance)
#
#
'''
Создайте класс GameCharacter с атрибутами name, health, level и методом speak(),
который выводит на печать 'Hi, my name is (значение атрибута name)'.
Создайте класс Villain, наследник класса GameCharacter с теми же атрибутами,
методом speak(), который выводит на печать 'Hi, my name is (значение атрибута name) and I will kill you',
методом kill(), который принимает в качестве параметра объект класса GameCharacter,
присваивает атрибуту health этого объекта значение 0 и  выводит на печать 'Bang-bang, now you're dead'
'''
import csv
import time
from functools import wraps


# class GameCharacter:
#     def __init__(self, name, health, level):
#         self.name = name
#         self.health = health
#         self.level = level
#
#     def speak(self):
#         print(f'Hi, my name is {self.name}')
#
#
# tor = GameCharacter('Boss', 100, 100)
#
#
# class Villian(GameCharacter):
#     def __init__(self, name, health, level):
#         GameCharacter.__init__(self, name, health, level)
#
#     def speak(self):
#         print(f'Hi, my name is {self.name} and I will kill you')
#
#     dd = GameCharacter
#
#     def kill(self):
#         tor.health = 0
#         print('Bang-bang, now you\'re dead')
#
#
# killer = Villian('Vilkill', 50, 20)
# tor.speak()
# killer.speak()
# print(tor.health)
# killer.kill()
# print(tor.health)


'''
Cоздайте класс Chain с атрибутом number_of_items.
Создайте два специальных метода в этом классе.
Первый должен при вызове встроенной функции print()
для объекта этого класса выводить 'Chain with (значение number_of_items) items'
Второй должен при вызове встроенной функции len()
для объекта этого класса возвращать значение number_of_items этого объекта
'''

# class Chain:
#     def __init__(self, number_of_items):
#         self.number_of_items = number_of_items
#
#     def __str__(self):
#         return f'Chain with {self.number_of_items} items'
#
#     def __len__(self):
#         return self.number_of_items
#
#
# cha = Chain(15)
# print(cha)
# print(len(cha))


# import math
# math.factorial(23232323)


'''
Создайте файл utils.py  и определите в нем 2 функции - get_favorite_color() и get_favorite_number().
Первая функция должна возвращать строку 'super-duper color', вторая должна возвращать число 13.
Создайте файл main_file.py и в нем 2 переменные - color и number. 
Присвойте значения этим переменным при помощи вызова функций get_favorite_color() и get_favorite_number(). Выведите значение переменных на экран
'''

# from utils import get_favourite_colour, get_favourite_number
# import utils
# print(get_favourite_colour('black'))
# print(get_favourite_number(11))
# print(utils.get_favourite_colour('green'))
# print(utils.get_favourite_number(324))
#
#
# import shelve
#
# monday_schedule = ['Math', 'Python', 'English', 'Running']
#
# with shelve.open('schedules') as schedules:
#     # schedules['monday_schedule'] = monday_schedule
#
#     temp_list = schedules['monday_schedule']
#     temp_list.append('Swimming')
#     schedules['monday_schedule'] = temp_list
#     print(schedules['monday_schedule'])
#     # for key in schedules:
#     #     print(key, schedules[key])


'''
Создайте функцию-генератор get_week_day(),
которая создаёт генератор, вырабатывающий один день недели за раз,
Дни недели должны начинаться с воскресенья и заканчиваться субботой.
current_day = get_week_day()
print(current_day.__next__()) # 'Sunday'
print(current_day.__next__()) # 'Monday'
print(current_day.__next__()) # 'Tuesday'
print(current_day.__next__()) # 'Wednesday'
print(current_day.__next__()) # 'Thursday'
print(current_day.__next__()) # 'Friday'
print(current_day.__next__()) # 'Saturday'
'''

# week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
#
# def get_week_day():
#     for day in week:
#         yield day
#
# current_day = get_week_day()
# for i in range(len(week)):
#     print(current_day.__next__())

# value_list = ['one', 'two']
# max_num = 5
# i = 0
# result_list = []
# while len(result_list) <= max_num:
#     if i >= len(value_list):
#         i = 0
#     result_list.append(value_list[i])
#     i += 1
# print(result_list)


# def even_odd():
#     value = 'even'
#     while True:
#         yield value
#         if value == 'even':
#             value = "odd"
#         else:
#             value = "even"


# def odd_even():
#     week = ['Monday', 'Tuesday']
#     _ = 0
#     while True:
#         if _ >= len(week):
#             _ = 0
#         yield week[_]
#         _ += 1
#
#
# test = odd_even()
# print(test.__next__())
# print(test.__next__())
# print(test.__next__())
# print(test.__next__())


'''
Создайте функцию, возвращающую генератор, бесконечно вырабатывающий квадраты целых чисел, начиная с 1.
infinite_square_generator = get_infinite_square_generator()
print(next(infinite_square_generator)) # 1
print(next(infinite_square_generator)) # 4
print(next(infinite_square_generator)) # 9
print(next(infinite_square_generator)) # 16
'''

# def squares_of_integers():
#     num = 1
#     while True:
#         if type(num) == int:
#             yield num**2
#             num += 1
#
#
# sqrs = squares_of_integers()
#
# print(next(sqrs))
# print(next(sqrs))
# print(next(sqrs))
# print(next(sqrs))
# print(next(sqrs))
# print(next(sqrs))


'''
Создайте функцию-декоратор print_args, 
которая распечатывает аргументы *args и **kwargs функции,
 которую она декорирует
'''

# def print_args(func):
#     def wrapper(*args, **kwargs):
#         func(*args)
#         print(*args, **kwargs)
#     return wrapper
#
#
# @print_args
# def leichak(a, b):
#     print(2)
#     return a, b
#
#
# leichak(1, 3)


'''
Создайте функцию-декоратор hello_from_decorator, 
которая добавляет к возвращаемому значению функции, 
которую она декорирует, строку "Hello from decorator!"
'''

# def hello_from_decorator(func):
#     def wrapper(*args, **kwargs):
#         return func(*args, **kwargs) + " " + "Hello from decorator!"
#
#     return wrapper
#
#
# @hello_from_decorator
# def some_text():
#     a = "So"
#     return a
#
#
# print(some_text())

'''
Создайте функцию-декоратор prohibit_more_than_2_args, 
которая выполняет функцию, которую она декорирует, 
если в этой функции не больше двух аргументов. 
В противном случае должна вызываться ошибка ValueError с сообщением "Function must have less than 3 arguments!"
'''

# def prohibit_more_than_2_args(func):
#     def wrapper(*args, **kwargs):
#         print(type(args))
#         print(len(args))
#         if len(args) > 2:
#             raise ValueError("Function must have less than 3 arguments!")
#         else:
#             return func(*args, **kwargs)
#     return wrapper
#
#
# @prohibit_more_than_2_args
# def decorated_func(a, b):
#     return a, b
#
#
# decorated_func(1, 2, 3)


'''
Создайте функцию-декоратор wait, которая задерживает выполнение кода функции, 
которую она декорирует, на n секунд. Аргумент n должен передаваться в декоратор. 
Воспользуйтесь функцией для задержки выполнения кода. 
Найдите информацию об использовании этой функции в интернете самостоятельно. 
Также после задержки должно выводится сообщение 
"There was a pause {количество секунд} seconds before execution {имя задекорированной функции }"
'''


# def wait(time_pause):
#     def inner_dec(func):
#         @wraps(func)
#         def wrapper(*args, **kwargs):
#             time.sleep(time_pause)
#             func()
#             return print(f"There was a pause {time_pause} seconds before execution {func.__name__}")
#
#         return wrapper
#
#     return inner_dec
#
#
# @wait(2)
# def exec_def():
#     print(5)
#
#
# exec_def()

'''
web scrapping task
'''
import requests
from bs4 import BeautifulSoup

# response = requests.get("http://quotes.toscrape.com/")
# html_data = BeautifulSoup(response.text)
# quotes = html_data.find_all(class_='quote')
# empty_list = []
# for quote in quotes:
#     empty_list.append(quote.find(class_='text').get_text().split(','))
# with open('quotes.csv', 'w') as file:
#     csv_writer = csv.writer(file)
#     for row in empty_list:
#         csv_writer.writerow(row)


'''
Создайте функцию add_student(), 
которая принимает в качестве параметров имя, фамилию и возраст и добавляет запись в файл students.csv. 
При необходимости воспользуйтесь поиском информации в интернете
'''


def add_student(name, surname, age):
    empty_list = [[name], [surname], [age]]
    with open('students.csv', 'w') as file:
        file_writer = csv.writer(file)
        for profile in empty_list:
            file_writer.writerow(profile)


add_student('oleksii', 'petlytskyi', '33')


def print_students():
    with open('students.csv') as f:
        read_csv = csv.reader(f)
        for stud in read_csv:
            print(stud)


print_students()


# CREATE TABLE students (
#     first_name TEXT,
#     second_name TEXT,
#     age INTEGER
# );