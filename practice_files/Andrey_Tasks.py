# # 1 task
# greeting = 'hello'
# print(greeting)
#
# # 2 task
# x = 7
# y = 9
# z = x + y
# print(z)
#
# # 3 task
# name = 'Oleksii'
# surname = 'Petlytskyi'
# patronymic = 'Ihorovych'
# print(name+' ('+surname+') '+patronymic)
#
# # 4 task
# pi = 3.14
# r = 10
# area = (r**2)*pi
# print(area)
#
# # 5 task
# # ширина
# b = 10
# # высота
# h = 2
# p = (b+h)*2
# print(p)
# b = 6
# p = (b+h)*2
# print(p)

# first_number = int(input('enter the first number: '))
# second_number = int(input('enter the second number: '))
# print(first_number+second_number)

# string = input('enter the string: ')
# number_of_char = int(input('select char: '))
# print(string[number_of_char-1])


# string = input('enter the string: ')
# first_char = int(input('enter the first number: '))
# last_char = int(input('enter the last number: '))
# print(string[first_char:last_char-1])

# num_1 = input('enter the number 1: ')
# num_2 = input('enter the number 2: ')
# num_3 = input('enter the number 3: ')
# num_4 = input('enter the number 4: ')
# num_5 = num_1 + num_4
# num_6 = num_2 + num_3
# print(int(num_5)+int(num_6))

# expression = input('enter the expression: ').split(' ')
# num_1 = int(expression[0])
# num_2 = int(expression[2])
# symbol = expression[1]
#
# if symbol == '+':
#     print(num_1+num_2)
# elif symbol == '-':
#     print(num_1 - num_2)
# elif symbol == '*':
#     print(num_1 * num_2)
# elif symbol == '/':
#     if num_2 == 0:
#         print('we cant divide on 0')
#     else:
#         print(num_1 / num_2)
# elif symbol == '%':
#     print(num_1 % num_2)
# else:
#     print('go drink some beer')
#
# poem = "i want to learn python"
# poen = "I dont want to lear python"
# if poem != poen:
#     poen = poem
#     print(poen)
# else:
#     print("loco")

# ---------------------palindrome----------------------------

# С клавиатуры вводится натуральное число. Найти его наибольшую цифру.
# Например, введено число 764580. Наибольшая цифра в нем 8.

# number = set(input("enter some digits: "))
# print(number)
# max_num = 0
# for num in number:
#     if int(num) > max_num:
#         max_num = int(num)
# print(max_num)

# С клавиатуры вводится натуральное число. Найти его наибольшую цифру.
# Например, введено число 764580. Наибольшая цифра в нем 8.

# print(max(set(input("enter some digits: "))))

# Сформировать из введенного числа
# обратное по порядку входящих в него цифр и вывести на экран.
# Например, если введено число 3486, то надо вывести число 6843.

# print("".join(list((reversed(input("type something: "))))))

# print(input("input: ")[::-1])

number = input("text: ")
n = ""
for i in range(len(number)-1, -1, -1):
    n += number[i]
print(n)