# сформировать список всех чисел в диапазоне от 10 до 20 которые четные
l = [number for number in range(10, 20) if number % 2 == 0]
print(l)

# сформировать список сумм всех таплов в которых присудствует 1
l = [(1, 2), (1, 4), (1, 6), (2, 3), (8, 10)]
sums = [sum(element) for element in l if 1 in element]
print(sums)

# найти сумму 1 и последнего элементов тапла который отсортирован
l = [(1, 3, 8), (1, 8, 3), (3, 8, 1), (2, 0, 1)]
sums = [element[0] + element[2] for element in l if element[0] < element[1] < element[2]]
print(sums)


data = [{'name': 'Andrey', 'age': 18}, {'name': 'Oleg', 'age': 30}, {'name': 'Anna', 'age': 8}]
l = [user['name'] for user in data if user['age'] >= 18]
print(l)

