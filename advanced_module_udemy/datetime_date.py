from datetime import datetime

year = input('enter the year: ')
month = input('enter the month: ')
day = input('enter the day: ')

date_string = f'{year}-{month}-{day}'
week_order = {1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday', 5: 'Friday', 6: 'Saturday', 7: 'Sunday'}


def guess_day():
    return datetime.strptime(date_string, "%Y-%m-%d").isoweekday()


day_num = guess_day()
print(date_string + " is a " + week_order[day_num])
