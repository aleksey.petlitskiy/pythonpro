import time

time_start = time.monotonic()


def quizz():
    score = 0
    while True:
        # 1 if
        if int(input("How old is your cat Nelson? ")) == 6:
            print('Excellent!')
            score += 1
        else:
            print('Not correct :( Hope you you will be a better mother!')
        # if 2
        if input("Where was you first kiss with your lovely husband in low? car or flat ") == 'car':
            print('Excellent!')
            score += 1
        else:
            print('Not correct :( Hope you you will be a better wife!')
        # if 3
        if input("Is your husband happy to be living with you? True or False ").casefold() == 'true':
            print('Excellent!')
            score += 1
        else:
            print('Not correct :( I hope you did it correctly! ')
        return score


time_end = time.monotonic()
print('Congratulation', f'Your family scoree is {quizz()} and total execution time is {time_end - time_start}')
