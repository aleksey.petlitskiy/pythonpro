# сравнить длину
# если не ровняется то добавить в начало или выдать ошибку


def decorator(func):
    def wrapper(*args, **kwargs):

        def format_numbers(numbers):
            result_list = []
            for elem in args:
                if 12 - len(elem) == 0:
                    result_list.append("+" + elem)
                elif 12 - len(elem) == 1 and elem[0] != "0":
                    result_list.append("+9" + elem)
                elif 12 - len(elem) == 2:
                    result_list.append("+91" + elem)
                elif elem[0] == "0":
                    result_list.append("+91" + elem[1:])
                else:
                    raise ValueError("Wooohhaaa")
            return result_list

        def add_spaces_to_numbers(numbers):
            correct_nums_list = []
            for n in numbers:
                correct_num = n[:3] + " " + n[3:7] + " " + n[7:]
                correct_nums_list.append(correct_num)
            return correct_nums_list

        formatted = format_numbers(args)
        formatted_with_spaces = add_spaces_to_numbers(formatted)

        return func(*formatted_with_spaces)

    return wrapper


@decorator
def mob_numbers(*args):
    return sorted(args)


print(mob_numbers('07895462130', '919875641230', '91987564123'))




