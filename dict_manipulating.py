"""
Нам нужно сделать 2 разных файла, в одном из них нужно создать словарь, при помощи генератора словаря,
во втором нужно написать класс, в котором будет несколько методов:
-- Первый метод меняет местами ключ и значение в словаре, - done
-- второй метод возвращает наибольший элемент в словаре. - done
-- Третий метод проверяет есть ли конкретный ключ в словаре
Далее в файле где определен словарь нужно вызвать все 3 метода
"""


class DictManipulating:
    def __init__(self, dictionary):
        self.dictionary = dictionary

    # swapping key -> value
    @staticmethod
    def swap_items_in_dict(dictionary: dict) -> dict:
        return {value: key for key, value in dictionary.items()}

    # finding a biggest value in dict
    @staticmethod
    def find_biggest_value(dictionary: dict):
        return sorted(dictionary.items(), key=lambda num: num[1])[-1][-1]

    # defining if element present in dict
    @staticmethod
    def define_key_in_dict(dictionary: dict, key):
        return key in dictionary

    # add self.dictionary to another dictionary
    def add_dict_to_dict(self, dictionary: dict) -> dict or Exception:
        if len(dictionary) == 0:
            raise Exception('Empty Dick')
        else:
            dictionary.update(self.dictionary)
            return dictionary