import pytest

from dict_manipulating import DictManipulating
from pytest_mock import mocker
dict_manipulating = DictManipulating


class DictManipulatingMock(mocker):
    def __init__(self):
        self.dictionary = {1: 2, 3: 4}


df = DictManipulatingMock()


class TestDictManipulating:
    def __init__(self):
        self.dictionary = {1: 2, 3: 4}

    # test swap_items_in_dict() method from DictManipulating class
    @pytest.mark.parametrize(
        'dictionary, expected',
        [
            ({1: 2, 2: 3, 4: 5}, {2: 1, 3: 2, 5: 4}),
            ({3: 2}, {2: 3})
        ]
    )
    def test_swap_items_in_dict(self, dictionary, expected):
        assert dict_manipulating.swap_items_in_dict(dictionary) == expected

    # test find_biggest_value() method from DictManipulating class
    @pytest.mark.parametrize(
        'dictionary, expected',
        [
            ({1: 2, 2: 3, 4: 5}, 5)
        ]
    )
    def test_define_biggest_value(self, dictionary, expected):
        assert dict_manipulating.find_biggest_value(dictionary) == expected

    # test define_key_in_dict() method from DictManipulating class
    @pytest.mark.parametrize(
        'dictionary, expected',
        [
            ({'q': 2, 'b': 3}, True)
        ]
    )
    def test_define_key(self, dictionary, expected):
        assert dict_manipulating.define_key_in_dict(dictionary, key='q') == expected

    # test add_dict_to_dict() method from DictManipulating class
    @pytest.mark.parametrize(
        'dict_2, expected',
        [
            ({'a': 'b', 'c': 'd'}, {2: 4, 4: 16, 6: 36, 8: 64, 'a': 'b', 'c': 'd'}),
            ({}, Exception)
        ]
    )
    def test_add_dict_to_dict(self, dict_2, expected):
        mocker.patch(DictManipulating, DictManipulatingMock)
        assert dict_manipulating.add_dict_to_dict(dict_2) == expected


