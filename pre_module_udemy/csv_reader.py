import csv

# with open('cars.csv') as file:
# 	csv_reader = csv.reader(file)
# 	next(csv_reader)
# 	for car in csv_reader:
# 		# print(car)
# 		print(f'{car[1]} {car[2]} costs {car[4]}')

# with open('cars.csv') as file:
# 	csv_reader = csv.reader(file)
# 	data_list = list(csv_reader)
# 	print(data_list)
	
# with open('cars.csv') as file:
# 	csv_reader = csv.DictReader(file)
# 	for car in csv_reader:
# 		# print(car)
# 		print(f'{car["Make"]} {car["Model"]} costs {car["Price"]}')

# with open('cars.csv') as f:
# 	csv_reader = csv.reader(f, delimiter=';')
# 	# csv_reader = csv.DictReader(f)
# 	for car in csv_reader:
# 		print(car[1])
		# print(f"{car['YEAR']} {car['Make']} Size {car['Size']}")
		# next(csv_reader)
		# print(car['MAKE'])
# with open('cars1.csv') as file:
# 	csv_reader = csv.DictReader(file,delimiter=";")
# 	for car in csv_reader:
# 		# print(car)
# 		print(f'{car["Make"]} {car["Model"]} is {car["Length"]} m')

# with open('cars1.csv') as file:
# 	csv_reader = csv.reader(file,delimiter=";")
# 	next(csv_reader)
# 	for car in csv_reader:
# 		# print(car)
# 		print(f'{car[1]} {car[2]} is {car[3]} m')

# with open('cars2.csv') as file:
# 	csv_reader = csv.reader(file, delimiter="|")
# 	next(csv_reader)
# 	for car in csv_reader:
# 		# print(car)
# 		print(f'{car[1]} {car[2]} is {car[3]} m')


with open('write.csv', 'w') as file:
	write_csv = csv.writer(file)
	write_csv.writerow(['name', 'second_name', 'age'])
	write_csv.writerow(['oleksii', 'petlytskyi', '33'])
#
#
# with open('write.csv') as file:
# 	read_csv = csv.DictReader(file)
# 	for person in read_csv:
# 		print(person['name'])

# YEAR,Make,Model,Size,(kW)

with open('cars.csv') as file:
	read_cars = csv.DictReader(file)
	cars_list = list(read_cars)

with open('additional_cars_info.csv', 'w') as file:
	headers_list = ['YEAR', 'Make', 'Model', 'Size', '(kW)']
	csv_writer = csv.DictWriter(file, fieldnames=headers_list)
	csv_writer.writeheader()
	for car in cars_list:
		csv_writer.writerow({
			'YEAR': car['YEAR'],
			'Make': car['Make'],
			'Model': car['Model'],
			'Size': car['Size'],
			'(kW)': car['(kW)'],
		})
print()