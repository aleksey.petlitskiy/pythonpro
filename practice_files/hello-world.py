
# hw map filter list comprehension
# разпечатать новый список из 1 и последних букв в словах
# s = 'hello world python'
# words = s.split(' ')
# result_list = []
# for word in words:
#     result_list.append(word[0] + word[len(word)-1])
# print(', '.join(result_list))

# map
# string = 'hello world python'
# words = string.split(' ')
# result_list = list(map(lambda word1: word1[0] + word1[len(word1)-1], words))
# print(', '.join(result_list))

# --------------------------------------------------------------------------

# разпечатать новый список из 3й буквы каждого слова
s = 'hello world python'
words = s.split(' ')
result_list = []
for word in words:
    result_list.append(word[2])
print(', '.join(result_list))


s = 'hello world python'
words = s.split(' ')
result_list = list(map(lambda word1: word1[2], words))
print(result_list)
# --------------------------------------------------------------------------

# инверитировать значения
numbers = [3, -1, 3, 5, -2]
result_numbers = list(map(lambda num: -num, numbers))
print(result_numbers)

