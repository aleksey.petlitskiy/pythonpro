import pytz
import datetime


def get_timezone1():
    while True:
        count = 0
        for country in pytz.country_names:
            print(country, pytz.country_names[country], pytz.country_timezones.get(country))
            count += 1
            if count >= 5:
                break
        request_tz = input('Please, enter two letters code to get a timezone or q to quit: ').upper()
        if request_tz == 'Q':
            break
        else:
            tz_country = pytz.timezone(pytz.country_timezones.get(request_tz)[0])
            print('Local time is', datetime.datetime.now(tz=tz_country))
            print('UTC time is', datetime.datetime.utcnow())


get_timezone1()
