# capital = list(map(lambda cap: cap.capitalize(), names))

# преобразовать дату в список квадратов
data = [2, 10, 12, 7, 3]
list_result = list(map(lambda num: num * num, data))
print(list_result)

# return positive nums
data = [2, 10, -12, 7, -3, 0]
result_list = list(filter(lambda num: num <= 0, data))
print(result_list)

# map + filter => отфильтровать и прибавтить 1
data = [2, 10, -12, 7, -3]
result_list = list(map(lambda num: num + 1, filter(lambda num: num > 0, data)))
print(result_list)

# map + filter => увеличить на 1 и отфильтровать
data = [2, 10, -12, 7, -3]
result_list = list(filter(lambda num: num > 0, map(lambda num: num + 1, data)))
print(result_list)


# удалить 3й символ
string = "qwerty"
result_string = string[:2] + string[3:]
print(result_string)

# 1 и последний поменять местами
string = "qwerty"
result_string = string[-1] + string[1:-1] + string[0]
print(result_string)

# positive to 1 and neg to 0
data = [2, 10, -12, 7, -3]
result_list = list(map(lambda num:  1 if num > 0 else 0, data))
print(result_list)

#


