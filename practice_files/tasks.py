# Square Every Digit


def square_digits(num):
    num = str(num)
    result_string = ""
    for digit in num:
         result_string += str(int(digit)**2)
    return int(result_string)

# The Western Suburbs Croquet Club has two categories of membership, Senior and Open. They would like your help with an application form that will tell prospective members which category they will be placed.
#
# To be a senior, a member must be at least 55 years old and have a handicap greater than 7. In this croquet club, handicaps range from -2 to +26; the better the player the lower the handicap.


# python
def open_or_senior(data):
    result_list = []
    for member_info in data:
        if member_info[0] >= 55 and member_info[1] > 7:
            result_list.append("Senior")
        else:
            result_list.append("Open")
    return result_list


# python with shorten if
def open_or_senior(data):
    result_list = []
    for member_info in data:
        result_list.append("Senior" if member_info[0] >= 55 and member_info[1] > 7 else "Open")
    return result_list


# list comprehension
def open_or_senior(data):
    return ["Senior" if member_info[0] >= 55 and member_info[1] > 7 else "Open" for member_info in data]


# map
def open_or_senior(data):
    return list(map(lambda member_info: "Senior" if member_info[0] >= 55 and member_info[1] > 7 else "Open", data))


# test
def dig_pow(n, p):
    n = str(n)
    sum_of_numbers = 0
    for digit in n:
        sum_of_numbers += int(digit) ** p
        p += 1
    if sum_of_numbers % int(n) == 0:
        return sum_of_numbers // int(n)
    return -1
