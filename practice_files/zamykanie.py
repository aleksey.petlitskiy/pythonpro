# реализовать ф-ю которая принимает флаг  флаг тру фолс дикримент инкримент и возваращает функцию котрая или уменьшает число на 1 или увеличивает на 1
def increment_decrement_decorator(number2, isIncrement):
    def increment(number):
        return number + number2

    def decrement(number):
        return number - number2

    return increment if isIncrement else decrement


first_function = increment_decrement_decorator(1, True)
first_function1 = increment_decrement_decorator(2, True)
second_function = increment_decrement_decorator(1, False)
second_function2 = increment_decrement_decorator(2, False)

print(first_function(1))
print(first_function1(2))
print(second_function(2))
print(second_function2(2))


# реализовать ф-ю котроая принмает индекс и флаг тру фолс котрый обозначает слева или справа получаем сивол по индексу
# функция возвращает фунцию котрая возвращает симвом под индексов слева или справа
def get_char_by_index_generator(index, isLeft):
    def get_symbol_from_left(string):
        return string[index]

    def get_symbol_from_right(string):
        return string[-index]

    return get_symbol_from_left if isLeft else get_symbol_from_right


first_function = get_char_by_index_generator(5, True)
second_function = get_char_by_index_generator(2, False)

print(first_function("string"))
print(second_function("string"))

# пройти лекцию внутринние ф-и, декораторы, замыкания


#Напишите функцию sum, которая работает таким образом: sum(a)(b) = a+b.
# Да, именно таким образом, используя двойные круглые скобки (не опечатка).
# Например:
# sum(1)(2) = 3
# sum(5)(-1) = 4



