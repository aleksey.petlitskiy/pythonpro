# import operator
# from functools import reduce
#
# #  Дан список чисел numbers = [1, -2, -4, 5, 9, 7, 6, -3, -10]. Найдите произведение
# #  всех четных отрицательных чисел. Решите задачу, используя
# #   a) map, filter, reduce
# #   b) list comprehension
# #   c) обычные Python-структуры (if, for и т.д.)
#
#
# # python
# numbers = [1, -2, -4, 5, 9, 7, 6, -3, -10]
# summary = 1
# for i in numbers:
#     if i < 0 and i % 2 == 0:
#         summary *= i
# print(summary)
#
# # filter
# numbers = [1, -2, -4, 5, 9, 7, 6, -3, -10]
# result = reduce(lambda acc, number: acc * number, filter(lambda number: number % 2 == 0 and number < 0, numbers), 1)
# print(result)

# # list comprehension
# numbers = [1, -2, -4, 5, 9, 7, 6, -3, -10]
# summary = [number for number in numbers if number % 2 == 0 and number < 0]
# result = reduce(operator.mul, summary, 1)
# print(result)
#
# # 2. Дан список имен names = ['Andrey', 'oleg', 'Igor', 'olga', 'semen']. Известно, что
# # любое имя должно быть записано с большой буквы. Преобразуйте этот список в
# # правильный, где все имена начинаются с большой буквы. Решите задачу, используя
# # a) map, filter, reduce
# # b) list comprehension
# # c) обычные Python-структуры (if, for и т.д.)
# # map
# names = ['Andrey', 'oleg', 'Igor', 'olga', 'semen']
# capital = list(map(lambda cap: cap.capitalize(), names))
# print(capital)
#
# # list comprehension
# names = ['Andrey', 'oleg', 'Igor', 'olga', 'semen']
# capital = [name.capitalize() for name in names]
# print(capital)
#
# # python
# names = ['Andrey', 'oleg', 'Igor', 'olga', 'semen']
# result_names = []
# for name in names:
#     result_names.append(name.capitalize())
# print(result_names)
#
# # 3. Пользователь вводит 3 числа. Найти максимальное среди этих чисел. Решите задачу,
# # используя:
# # а) функции map, filter, reduce
# # b) обычные Python-структуры (if, for)
#
# first = int(input())
# second = int(input())
# third = int(input())
#
# if first > second and first > third:
#     print(first)
# elif second > first and second > third:
#     print(second)
# else:
#     print(third)
#
#
# numbers = [first, second, third]
# max_value = first
# for number in numbers[1:]:
#     if number > max_value:
#         max_value = number
#
#
# # Есть список строк strings = ['cat', 'hello', 'world', 'dog', 'citten', 'py', 'python']. Отобрать строки,
# # длина которых не превышает 3 и записать их в новый список в обратном порядке.
# # ['tac', 'god', 'yp'].
# #  Решите задачу, используя
# # a) map, filter, reduce
# # b) list comprehension
# # c) обычные Python-структуры (if, for и т.д.)
#
# # filter + map
# strings = ['cat', 'hello', 'world', 'dog', 'citten', 'py', 'python']
# strings1 = list(map(lambda string: ''.join(reversed(string)), filter(lambda string: len(string) <= 3, strings)))
# print(strings1)
#
# # list comprehension
# strings = ['cat', 'hello', 'world', 'dog', 'citten', 'py', 'python']
# strings1 = [''.join(reversed(string)) for string in strings if len(string) <= 3]
# print(strings1)
#
# # python
# strings = ['cat', 'hello', 'world', 'dog', 'citten', 'py', 'python']
# strings1 = []
# for string in strings:
#     if len(string) <= 3:
#         strings1.append(''.join(reversed(string)))
# print(strings1)

# 5. Есть список людей people = [{'name': 'Andrey', 'age': 21}, {'name': 'Oleg', 'age': 22}, {'name': 'Ostap', 'age': 30},
# {'name': 'Rusik', 'age': 55}, {'name': 'Pronka', 'age': 20}, {'name': 'Varvara Stepanovna', 'age': 140}]
# Выведите строку, в которой через запятую записаны имена людей, не младше 30 лет.  Решите задачу,
# используя:
# а) функции map, filter, reduce
# b) обычные Python-структуры (if, for)

people = [{'name': 'Andrey', 'age': 21}, {'name': 'Oleg', 'age': 22}, {'name': 'Ostap', 'age': 30}, {'name': 'Rusik', 'age': 55}, {'name': 'Pronka', 'age': 20}, {'name': 'Varvara Stepanovna', 'age': 140}]


def filter_people_func(person):
    return person["age"] >= 30


# filter_people = list(map(lambda person: person["name"], filter(filter_people_func, people)))
filter_people = list(map(lambda person: person["name"], filter(lambda person: person["age"] >= 30, people)))
result = ", ".join(filter_people)
print(result)

# python
people = [{'name': 'Andrey', 'age': 21}, {'name': 'Oleg', 'age': 22}, {'name': 'Ostap', 'age': 30}, {'name': 'Rusik', 'age': 55}, {'name': 'Pronka', 'age': 20}, {'name': 'Varvara Stepanovna', 'age': 140}]
filter_people = []
for person in people:
    if person["age"] >= 30:
        filter_people.append(person["name"])
result = ", ".join(filter_people)
print(result)







