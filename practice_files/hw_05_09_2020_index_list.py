# dz #1
# удалить все двойки с листа, используя remove()
numbers = [1, 2, 3, 4, 5, 6, 2, 3, 2, 1, 2, 2, 6, 7, 8]
result_list = []
for num in numbers:
    if num != 2:
        result_list.append(num)
print(result_list)

# удалить все двойки с листа, используя remove()
numbers = [1, 2, 3, 4, 5, 6, 2, 3, 2, 1, 2, 2, 6, 7, 8]
numbers_of_two = numbers.count(2)
for _ in range(numbers_of_two):
    numbers.remove(2)
print(numbers)


# dz #2 # индексы двоек
numbers = [1, 4, 3, 4, 5, 6, 2, 3, 2, 1, 2, 2, 6, 7, 8]
result_list = []
start = 0
for number in numbers:
    if number == 2:
        result_list.append(numbers.index(number, start))
        start = numbers.index(number, start) + 1
print(result_list)



