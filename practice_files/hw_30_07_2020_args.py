from functools import reduce
# 1. Реализовать функцию, которая принимает неограниченное количество чисел и возвращает произведение
# этих чисел. Если в функцию не передано ни одного числа, возвратить строку "Передайте хотя бы одно число".


def get_multiplication(*numbers):

    # if len(numbers) == 0:
    #     return "Передайте хотя бы одно число"
    # return reduce(lambda acc, number: acc * number, numbers)

    return "Передайте хотя бы одно число" if len(numbers) == 0 else reduce(lambda acc, number: acc * number, numbers)


print(get_multiplication(1, 2, 3))


# 2. Реализовать функцию, которая принимает неограниченное количество строк, объединяет их через запятую
# и возвращает результирующую строку.
# Например:
# f ('a', 'b', 'c') --> 'a, b, c'

def join_strings(*strings):
    # if len(strings) > 2:
    #     return ", ".join(strings)
    # return ". ".join(strings)

    return ", ".join(strings) if len(strings) > 2 else ". ".join(strings)


print(join_strings("andrey", "aleksey"))
