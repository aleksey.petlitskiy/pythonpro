# # 1. посчитать сколько каждого из символов находится в стринге
#
#
# def count_symbols(string):
#     result_string = list(string)
#     res_sym = {}
#     for symbol in result_string:
#
#
# # return res_sym{symbol: number}
#
#
# print(count_symbols("aaabbbccc"))

# task about list --> string
dialog_users = {'chat_name': 'Java', 'users': ['Andrey', 'Oleg', 'Oleksiy', 'Roman', 'Oleshko']}
# 5 users in the chat Python: Andrey, Oleg, Oleksiy, Roman, Oleshko
title = ', '.join(dialog_users['users'])
print(str(len(dialog_users['users'])) + " users in the chat " + dialog_users['chat_name'] + ': ' + title)

# task dict --> int
temps = {
  'values': {
    'min_temp': 145,
    'max_temp': 567,
    'temp': 160,
  },
  'date': '25-08-2020'}
# Temperature at 25-08-2020 is <average temp>
print('Temperature at ' + temps['date'] + " is " + str((temps['values']['min_temp'] + temps['values']['max_temp'] + temps['values']['temp']) // 3))

# task list --> dict
l = [123, 76, 43, 98, 22, 10]
# {'greater': 34, 'less': 11}
data = {'greater': 0, 'less': 0}
for user_input in l:
  if user_input > 50:
    data['greater'] += 1
  else:
    data['less'] += 1
print(data)



