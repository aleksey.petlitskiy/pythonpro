import requests
import sqlite3
from bs4 import BeautifulSoup

# response = requests.get(url, headers={'Accept': 'application/json'}, params={
#     'format': 'geojson',
#     'starttime': '2019-01-01',
#     'endtime': '2019-01-02',
#     'latitude': '47.63333',
#     'longitude': '26.25',
#     'maxradius': '180',
# })
#
# data = response.json()
# print(data)


# data_list = []
# def append_data(func):
#     def wrapper(a=0, *args, **kwargs):
#         while True:
#             yield data_list.append(input())
#             a += 1
#     return wrapper
#
#
# @append_data
# def get_data(starttime, endtime, latitude, c, maxradius):
#     response_data = requests.get(url, headers={'Accept': 'application/json'}, params={
#         'format': 'geojson',
#         'starttime': f'{starttime}',
#         'endtime': f'{endtime}',
#         'latitude': f'{int(latitude)}',
#         'longitude': f'{int(longitude)}',
#         'maxradius': f'{int(maxradius)}'})
#     return response_data.json()

url = "https://earthquake.usgs.gov/fdsnws/event/1/query?"


def append_data(func):
    def wrapper():
        while True:
            try:
                starttime, endtime, latitude, longitude, maxradius = '', '', '', '', ''
                new_data_range = [input("Eneter the start time: " f"{starttime}"),
                                  input("Eneter the end time: " f"{endtime}"),
                                  input("Eneter the end latitude: " f"{latitude}"),
                                  input("Eneter the end longitude: " f"{longitude}"),
                                  input("Eneter the end maxradius: " f"{maxradius}")]
                return func(new_data_range)
            except ValueError as e:
                print(f"Wrong data format were entered: \n {e}")

    return wrapper


@append_data
def get_data(new_data_range):
    starttime, endtime, latitude, longitude, maxradius = new_data_range
    response_data = requests.get(url, headers={'Accept': 'application/json'}, params={
        'format': f'geojson',
        'starttime': f'{starttime}',
        'endtime': f'{endtime}',
        'latitude': f'{latitude}',
        'longitude': f'{longitude}',
        'maxradius': f'{maxradius}'})
    return response_data.json()


def add_earthquake_data_to_db(earthquake_place_list):
    conn = sqlite3.connect('earthquake_db.dp')
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE earthquake (id TEXT, place TEXT, magnitude TEXT);")
    cursor.executemany("INSERT INTO earthquake VALUES (?, ?, ?)", earthquake_place_list)
    conn.commit()
    conn.close()


data = get_data()
data_earthquake_list = data['features']
count = 0
earthquake_list = []
for earthquake in data_earthquake_list:
    count += 1
    earthquake_list.append([count, earthquake['properties']['place'], earthquake['properties']['mag']])


add_earthquake_data_to_db(earthquake_list)