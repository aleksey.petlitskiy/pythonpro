import time
from collections import namedtuple

# Puple = namedtuple()

puple = time.gmtime()
print(time.gmtime(), ' = gmtime')
print(time.time(), ' = time')
print(time.localtime(), ' = localtime')
print(puple.tm_yday, ' = named tuple')
print(time.ctime(time.time()), ' = local time')
