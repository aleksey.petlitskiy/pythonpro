from tkinter import *

root_window = Tk()

root_window.geometry('700x500')
root_window.title('this is the very first title'.upper())
# create a widget/label
hello_label = Label(root_window,
                    text='The very first hello in tkinter',
                    font=('Al Bayan', 20, 'bold'),
                    fg='green',
                    bg='#C48CAB',
                    relief='solid')
version_label = Label(root_window,
                    text=f'TkVersion is {TkVersion}')
go_button = Button(root_window,
                    text='GO BUTTON',
                    font=('Al Bayan', 30, 'bold'),
                    fg='red',
                    highlightbackground='black',
                    relief='solid')
# put the widget on the root window
hello_label.pack()
version_label.place(x=350, y=250)
go_button.place(y=250)
root_window.mainloop()

