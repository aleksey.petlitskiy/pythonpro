import operator
# Реализовать функцию, которая принимает 2 числа и знак и возвращает результат сложения, вычитания и т.д.
# И с помощью /pytest покрыть функцию тестами

# expression_ = input("print some expression you want to calculate: "
#                    "ex.:(1 + 1): ")
# result = 0
# if '+' or '*' or '/' or '-' in expression_:
#     result = int(expression_[0]) + expression_[1] + int(expression_[-1])
#     # result = eval(expression)
# print(result)


def calculator(expression):
    if '+' in expression:
        return int(expression[0]) + int(expression[-1])
    elif '-' in expression:
        return int(expression[0]) - int(expression[-1])
    elif '*' in expression:
        return int(expression[0]) * int(expression[-1])
    elif '/' in expression:
        return int(expression[0]) / int(expression[-1])
    elif '//' in expression:
        return int(expression[0]) // int(expression[-1])
    elif '%' in expression:
        return int(expression[0]) % int(expression[-1])

