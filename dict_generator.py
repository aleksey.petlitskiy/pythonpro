from dict_manipulating import DictManipulating

"""
Нам нужно сделать 2 разных файла, в одном из них нужно создать словарь, при помощи генератора словаря,
во втором нужно написать класс, в котором будет несколько методов:
-- Первый метод меняет местами ключ и значение в словаре,
-- второй метод возвращает наибольший элемент в словаре.
-- Третий метод проверяет есть ли конкретный ключ в словаре
Далее в файле где определен словарь нужно вызвать все 3 метода
"""


def dict_gen():
    return {num: num ** 2 for num in range(1, 10) if num % 2 == 0}


dict_manipulation = DictManipulating(dict_gen())
# print(dict_manipulation.swap_items_in_dict(dict_gen()))
# print(dict_manipulation.find_biggest_value(dict_gen()))
# print(dict_manipulation.define_key_in_dict(dict_gen(), 4))
print(dict_manipulation.add_dict_to_dict({'q': 5, 'b': 3, 'one': {'inner': 'dict'}}))
