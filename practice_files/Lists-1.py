cities = ['Kiev', 'Kharkov', 'Zholtye Vody', 'poltava', 'Odessa']
print(cities)  # print list
print(len(cities))  # to check the length of the list
print(cities[3])  # to print the third element in the list
print(cities[-1])  # to print the last element in the list
print(cities[-2].title())  # to print the penultimate element in the list

cities[1] = "Lvov"  # to replace the second element in list with another statement
print(cities[1])
print(cities)

# to add an element in the end of the list
cities.append("Dnipro")
print(cities)

# to add an element in the middle of the list
cities.insert(1, "Donetsk")
print(cities)

# to del element in the list
del cities[-1]
print(cities)

cities.remove("poltava")
print(cities)

# list sorting
cities.reverse()
print(cities)

