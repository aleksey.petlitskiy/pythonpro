# # 2. написать генератор, который будет выводить бесконечный ряд целых чисел (1, 2, 3, 4, 5 ....)
# def generator(start):
#     while True:
#         yield start
#         start += 1
#
#
# for num in generator(start=1):
#     print(num)

# # ---------------------------------------------------------------------------------------|
# 3. написать итератор, который будет возвращать четные числа, в случае если число < 10
# my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# iter_obj = iter(my_list)
#
# for num in iter_obj:
#     if num <= 10 and num % 2 != 0:
#         print(next(iter_obj))

# # ---------------------------------------------------------------------------------------|
# # 4. написать то же самое используя генератор
# def generator():
#     for num in range(1, 10+1):
#         if num <= 10 and num % 2 == 0:
#             yield num
#
#
# gen = generator()
# for i in range(1, 10 + 1):
#     print(next(gen))

# # ---------------------------------------------------------------------------------------|
# # 5. написать генератор, который будет возвращать по одной строчек в файле (формат самого файла не так важен, как и размер)

def read_file(fpath: str):
    with open(fpath, 'r') as file:
        yield [item.strip() for item in file]  # list comprehension для прикола


# # without exception
# file_path = './iteration_files/itergen.txt'
# for line in read_file(file_path):
#     print(line)
# print(next(read_file(file_path)))
# # exception before output
# path_to_file = read_file(file_path)
# for line in range(11):
#     print(next(path_to_file))

# # exception after output
# i = 0
# while i <= 11:
#     print(next(path_to_file))
#     i += 1

# # 6. прочитал ниче не понял

'''
7. написать класс cars, который будет содержать в себе параметры машин (айди, цвет, модель, тип двигателя, тип кузова, владельца, массу, процент повреждения)
7.1. создать метод str который будет возвращать всю информацию о машине
7.2. создать метод repr который будет выводить массу и модель машины
7.3. унаследовать класс cars для создания нового класса pickups в котором будут содержаться пикапы (тип кузова должен быть переопределен в пикап)
'''


class Car:
    def __init__(self, car_id, colour, model, engine_type, body_type, owner, weight, damage_percentage):
        self.car_id = car_id
        self.colour = colour
        self.model = model
        self.engine_type = engine_type
        self.body_type = body_type
        self.owner = owner
        self.weight = weight
        self.damage_percentage = damage_percentage

    def __str__(self):  # better to return str ---------------------!!!!!
        return \
            self.car_id, \
            self.colour, \
            self.model, \
            self.engine_type, \
            self.body_type, \
            self.owner, \
            self.weight, \
            self.damage_percentage

    def __repr__(self):
        print(
            self.weight,
            self.model
        )


class Pickups(Car):
    def __init__(self, car_id, colour, model, engine_type, owner, weight, damage_percentage):
        self.body_type = 'pickup'
        Car.__init__(self, car_id, colour, model, engine_type, self.body_type, owner, weight, damage_percentage)


car = Car(car_id=None, colour=None, model='Mazda CX-5', engine_type=None, body_type=None, owner=None, weight=2,
          damage_percentage=None)

pickups = Pickups(car_id=None, colour=None, model='Mazda CX-5', engine_type=None, owner=None,
                  weight=2, damage_percentage=None)

car.__repr__()
