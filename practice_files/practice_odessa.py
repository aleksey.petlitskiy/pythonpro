# from operator import itemgetter, attrgetter, methodcaller
# # Задача 1
# # Выведите все элементы, которые меньше 5.
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# lower_5 = list(filter(lambda number: number < 5, a))
# print(lower_5)
#
# # Задача 2
# # Нужно вернуть список, который состоит из элементов, общих для этих двух списков.
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
# result_list = list(filter(lambda number: number in b, a))
# print(result_list)
#
# # Задача 3
# # Отсортируйте list по значению в порядке возрастания и убывания.
# a = [6, 1, 8, 3, 5, 8, 6346, 21, 34, 456, 89]
# increasing = sorted(a, reverse=True)
# print(increasing)
# a.sort(reverse=True)
# print(a)
# print(increasing[::-1])
# print(list(reversed(increasing)))
#
# # Задача 4
# # Напишите программу для слияния нескольких словарей в один.
# a = {"number1": 1, "number2": 2}
# b = {"number3": 3, "number4": 4}
# c = list(a.items()) + list(b.items())
# print(dict(c))
# a.update(b)
# print(a)
#
# # Задача 5
# # Найдите три ключа с самыми высокими значениями в словаре.
# # 1
# my_dict = {'a': 500, 'b': 5874, 'c': 560, 'd': 400, 'e': 5874, 'f': 20}
# biggest_values = list(my_dict.items())
# biggest_values.sort(key=lambda i: i[1])
# print(dict(biggest_values[-3:]))
# # 2
# my_dict = {'a': 500, 'b': 5874, 'c': 560, 'd': 400, 'e': 5874, 'f': 20}
# print(dict(sorted(list(my_dict.items()), key=lambda i: i[1])[-3:]))
# # 3
# my_dict = {'a': 500, 'b': 5874, 'c': 560, 'd': 400, 'e': 5874, 'f': 20}
# print(dict(sorted(list(my_dict.items()), key=itemgetter(1))[:3]))
# print(dict(sorted(list(my_dict.items()), key=itemgetter(1), reverse=True)[:3]))
#
# # Задача 6
# # Отсортируйте словарь по значению в порядке возрастания и убывания.
# my_dict = {'f': 500, 'c': 5874, 'a': 560, 'f': 400, 'd': 5874, 'b': 20}
# print(list(my_dict.items()))
# print(dict(sorted(list(my_dict.items()), key=itemgetter(1), reverse=True)))
# print(dict(sorted(list(my_dict.items()), key=lambda t: t[1])))
#
#
# # Задача 8
# # Напишите проверку на то, является ли строка палиндромом.
# def check_if_palindrome(string):
#     print("palindrome") if string[::-1] == string else print("not palindrome")
#
#
# check_if_palindrome("aaaaaaaaa")
#
# # Задача 9
# # Вы принимаете от пользователя последовательность чисел, разделённых запятой.
# # Составьте список и кортеж с этими числами.
# # 1
# # user_input = input("type some numbers divided by coma: ").split(",")
# # print(user_input)
# # print(tuple(user_input))
# # 2
# # user_input = input("type some numbers divided by space: ")
# # user_input = " ".join(user_input)
# # print(list(user_input.split(" ")))
# # print(tuple(user_input.split(" ")))
#
#
# # Задача 10
# # Выведите первый и последний элемент списка.
# lst = [1, 2, 3, 4, 5, 6]
# print(lst[0])
# print(lst[-1:])
#
#
# # Задача 11
# # Напишите программу, которая принимает имя файла и выводит его расширение.
# # Если расширение у файла определить невозможно, выбросите исключение.
# file = input("input file name with its type: ")
# file_type = [print("file type is " + file.split(".")[1]) if "." in file else print("file type error")]
#
#
# # Задача 12
# # При заданном целом числе n посчитайте n + nn + nnn.
# number = 5
# for number in range(1, 3):
#     number += number
# print(number)
# # ????????
#
#
# # Задача 13
# # Напишите программу, которая выводит чётные числа из заданного списка и останавливается, если встречает число 237.
# list_numbers = [2, 5, 1, 3, 4, 7, 8, 9, 15, 237, 55, 66]
# new_list = []
# for i in list_numbers:
#     if i % 2 == 0:
#         new_list.append(i)
#     elif i == 237:
#         break
#
# print(new_list)
#
#
# # Задача 14
# # Напишите программу, которая принимает два списка и выводит все элементы первого, которых нет во втором.
# # result_list = list(filter(lambda number: number in b, a))
# list_1 = [12, 2, 5, 1, 3, 4, 7, 8, 9, 15, 237, 55, 66, 16]
# list_2 = [2, 5, 1, 3, 4, 7, 8, 9, 15, 237, 55, 66]
# result_list = list(filter(lambda num: num not in list_2, list_1))
# print(result_list)
#
#
# # Задача 15
# # Выведите список файлов в указанной директории.
#
#
# # Задача 17
# # Сложите цифры целого числа.
# number = 123
# i = list(str(number))
# b = int(i[0]) + int(i[1]) + int(i[2])
# print(b)
#
# str = "123"
# num = 0
# for b in str:
#     num += int(b)
# print(num)
#
#
# # Задача 18
# # Посчитайте, сколько раз символ встречается в строке.
# string = "aaassdddddasdnlasdasdas"
# letters_counts = {letter: string.count(letter) for letter in set(string)}
# print(letters_counts)
# # подсмотрел в книге
#
#
# # Задача 19
# # Поменяйте значения переменных местами.
# a = 5
# b = 1
# a = a + b
# b = a - b
# a = a - b
# print(a, b)
# # подсмотрел в инете
#
#
# # Задача 20
# # С помощью анонимной функции извлеките из списка числа, делимые на 15.
# lst = [75, 15, 90, 4, 6, 375]
# result_list = list(filter(lambda number1: number1 % 15 == 0, lst))
# print(result_list)
#
#
# # Задача 21
# # Нужно проверить, все ли числа в последовательности уникальны.
# # 1 by comparing list to set
# list_of_unique_numbers = input("input random numbers by coma: ")
# if list(list_of_unique_numbers) != sorted(set(list_of_unique_numbers)):
#     print("numbers in list are not unique")
# else:
#     print("numbers in list are unique")
#
# # 2 by comparing length of list and set
# list_of_unique_numbers = list(input("input random numbers by coma: "))
# if len(list_of_unique_numbers) != len(set(list_of_unique_numbers)):
#     print("numbers in list are not unique")
# else:
#     print("numbers in list are unique")
#
# # перебором не пойму как решить, почитал ничего не понял
#
#
# # Задача 22
# # Напишите программу, которая принимает тексти выводит два слова:
# # наиболее часто встречающееся и самое длинное.
# text = "Перевод не дословный. Я постарался подробнее объяснить некоторые моменты, которые могут быть непонятны\
# Если вы собрались изучать язык Python, но не можете найти подходящего руководства, то эта\
# статья вам очень пригодится! За короткое время, вы сможете познакомиться с\
# основами языка Python. Хотя эта статья часто опирается\
# на то, что вы уже имеете опыт программирования, но, я надеюсь, даже новичкам\
# этот материал будет полезен. Внимательно прочитайте каждый параграф. В связи с\
# сжатостью материала, некоторые темы рассмотрены поверхностно, но содержат весь\
# необходимый метриал."
# common_word = {word: text.split(" ").count(word) for word in text.split(" ")}
# print("the most common word is :", list(dict(sorted(list(common_word.items()),
#                                                     key=itemgetter(1), reverse=True)).items())[0][0])
# longest_word = {word: len(word) for word in text.split(" ")}
# print("the longest word is :", list(dict(sorted(list(longest_word.items()),
#                                                     key=itemgetter(1), reverse=True)).items())[0][0])
#


# 23 our mission here is to create a function that gets a tuple and returns a tuple with 3 elements - the first, third and second element from the last for the given array.
def easy_unpack(test):

    return test[-4:-1]


if __name__ == '__main__':
    print('Examples:')
    print(easy_unpack((1, 2, 3, 4, 5, 6, 7, 9)))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert easy_unpack((1, 2, 3, 4, 5, 6, 7, 9)) == (1, 3, 7)
    assert easy_unpack((6, 2, 9, 4, 3, 9)) == (6, 9, 3)
    assert easy_unpack((1, 1, 1, 1)) == (1, 1, 1)
    assert easy_unpack((6, 3, 7)) == (6, 7, 3)
    print('Done! Go Check!')


