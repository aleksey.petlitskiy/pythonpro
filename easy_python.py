# lines = ['a', 'b', 'c', 'd']
# new_str = '\n'.join(lines)
# print(new_str)
# import time

# dictionary = {
#     'name': 'Oleksii',
#     'age': 32,
#     'sex': 'male'
# }
# print(dictionary['name'])
#
#
# macbook = 13, 'laptop', 2020
# screen_size, pc_type, year = macbook
# print(screen_size, pc_type, year)
# new_list = [str(screen_size), pc_type, str(year)]
# test_string = '\n'.join(new_list)
# print(test_string)
#
# unicum = {'a', 'b', 'c', 'd', 'a'}
# print(unicum)

# letters_set = set('Создайте множество при помощи функции set() '
#                   'из текста, чтобы получить уникальные символы, '
#                   'содержащиеся в тексте.')
# print(letters_set)
# print(input())

# a = 1
# b = 2
# print(a == b, a != b, a > b, a < b, a >= b)
#
# a = 'a'
# b = 'A'
# print(a > b, '\n', f'ASCII numbers of letters "a" and "A": {ord(a), ord(b)}')

# Напишите код, который выводит сообщение: 'Enter any number'. Если было введено число 7, должно выводиться сообщение:
# '7 is a lucky number! Today is your lucky day!', если введено что-то другое - должно выводиться сообщение 'Thank you! Have a nice day!'

# greetings = input('Enter any number: ')
# if greetings == '7':
#     print('7 is a lucky number! Today is your lucky day!')
# else:
#     print('Thank you! Have a nice day!')


# Напишите код, который выводит сообщение:
# 'Enter an integer number'. Если было введено чётное число, должно выводиться сообщение:
# 'The number is even', если было введено нечётное число, должно выводиться сообщение 'The number is odd'

# greetings = int(input('Enter an integer number: '))
# if greetings % 2 == 0:
#     print(f'The number {greetings} is even')
# else:
#     print(f'The number {greetings} is odd')


# Используйте цикл for для вычисления суммы всех чётных чисел в диапазоне
# от 10 до 30 (включая крайние значения). Выведите результат на печать

# int_sum = 0
# for i in range(10, 31):
#     print(i)
#     if i % 2 == 0:
#         int_sum = int_sum + i
# print(int_sum)


# Получите от пользователя число на вводе и используйте
# цикл for для вывода на экран текста 'Hello!' столько же раз

# for _ in range(int(input())):
#     time.sleep(0.5)
#     print('Hello!')

# Из исходного списка greetings = ['hello', 'hi', 'hey', 'hola'] создайте новый список содержащий вторую букву из каждой строки исходного списка.
# Выведите новый список на печать.

# Решите задание двумя способами - при помощи List Comprehension и без него.

# sec_letters_list = []
# greetings = ['hello', 'hi', 'hey', 'hola']
# for letter in greetings:
#     sec_letters_list.append(letter[1])
# print(sec_letters_list)
#
# greetings = ['hello', 'hi', 'hey', 'hola']
# sec_letters_list = [letter[1] for letter in greetings]
# print(sec_letters_list)

# Из исходного списка digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] создайте новый список содержащий нечетные числа исходного списка.
# Выведите новый список на печать.
# Решите задание двумя способами - при помощи List Comprehension и без него.

# odd_numbers = []
# digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# for odd_num in digits:
#     if odd_num % 2 != 0:
#         odd_numbers.append(odd_num)
# print(odd_numbers)
#
# digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# odd_numbers = [odd_num for odd_num in digits if odd_num % 2 != 0]
# print([odd_num for odd_num in digits if odd_num % 2 != 0])

# smile = '\U0001f600'
# triangle = []
# for _ in range(10):
#     triangle.append(smile)
#     print(''.join(triangle))
# #
#
# smile = '\U0001f600'
#
# for number in range(10):
#     tria = ''
#     count = 0
#     while count <= number:
#         tria += smile
#         count += 1
#     print(tria)
#
# for number in range(11):
#     triangle = ''
#     for count in range(number):
#         triangle += smile
#     print(triangle)

# list_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
#
# new_list = []
# numbers = ''
# settings = set()
# for inner_list in list_list:
#     for num in inner_list:
#         new_list.append(num)
#         numbers += str(num)
#         settings.add(num)
# print(new_list, '\n', numbers, '\n', settings)

# Создайте функции cat_voice() and dog_voice(),
# которые выводят на экран 'Meow!' и 'Woof!' соответственно.
# Сделайте по одному вызову каждой из функций
#
# def cat_voice(text_cat=''):
#     print(text_cat)
#
#
# def dog_voice(text_dog=''):
#     print(text_dog)


# cat_voice('Meow!')
# dog_voice('Woof')

# Создайте функции cat_voice() and dog_voice(),
# которые возвращают значения 'Meow!' и 'Woof!' соответственно.
# Выведите на экран 'Meow!' и 'Woof!' по 2 раза


# for _ in range(2):
#     cat_voice('Meow!')
#     dog_voice('Woof')
#
# i = 0
# while i <= 1:
#     cat_voice('Meow!')
#     dog_voice('Woof')
#     i += 1


# Создайте функцию get_voice()
# которая возвращает передаваемый ей в качестве параметра
# текст c восклицательным знаком.


# def get_voice(text):
#     return text
# print(get_voice('Meow!'))


# Создайте функцию, которая генерирует последовательность нечетных чисел в диапазоне от a до b (a и b включительно).
# Значения a и b должны передаваться в качестве параметров.
# Результирующая последовательность должна возвращаться в форме объекта list.
# Решите задание двумя способами - при помощи List Comprehension  и без него


# odd_number_list = []
#
#
# def generate_odd_number_list(a, b):
#     for number in range(a, b):
#         if number % 2 != 0:
#             odd_number_list.append(number)
#     return odd_number_list
#
#
# print(generate_odd_number_list(1, 11))


# def generate_odd_number_list(a, b):
#     odd_number_list = [number for number in range(a, b) if number % 2 == 1]
#     return odd_number_list
#
#
# print(generate_odd_number_list(1, 11))


# def animal_sounds(karamba, **kwargs):
#     if "cow" in kwargs:
#         print(f'{karamba}, the cow says {kwargs["cow"]}')
#     else:
#         print("There no such animal! Who are you?")
#
#
# animal_sounds('Karamba', cow='Bark')


# Создайте функцию is_cat_here(), которая принимает любое количество аргументов и проверяет есть ли строка 'cat' среди них.
# Функция должна возвращать True, если такой параметр есть и False в обратном случае.
# Буквы в строке 'cat' могут быть как большие, так и маленькие

# def is_cat_here(*args):
#     if "CaT".lower() in args:
#         return True
#     else:
#         return False
#
#
# print(is_cat_here('cat', 'rat', 'slut'))


'''
Создайте функцию is_item_here(item, *args), которая проверяет есть ли  item среди args. 
Функция должна возвращать True, 
если такой параметр есть и False в обратном случае.
'''

# def is_items_here(item, *args):
#     if item in args:
#         return True
#     else:
#         return False
#
#
# print(is_items_here('photo', 'photo', 'cup', 'picture'))


'''
Создайте функцию your_favorite_color() с позиционным параметром my_color и параметрами **kwargs, 
которая будет выводить на экран 'My favorite color is (значение my_color), what is your favorite color?', 
если в параметрах kwargs нет ключа color, и 'My favorite color is (значение my_color), but (значение по ключу color) is also pretty good!', 
если в параметрах kwargs ключ color присутствует
'''

# def your_favorite_color(my_color, **kwargs):
#     if 'color' not in kwargs:
#         print(f'My favorite color is {my_color}, what is your favorite color?')
#     else:
#         print(f'My favorite color is {my_color}, but {kwargs["color"]} is also pretty good!')
#
#
# your_favorite_color('Pink', color='Green', heith='189', length='230')


# def num_plus_num(x):
#     return x * x
#
#
# numbers = [1, 2, 3, 4, 5, 6]
#
# print(set(map(num_plus_num, numbers)))


# for num in map(num_plus_num, numbers):
#     print(num)


# def sum_two_number(x, y, r):
#     return x + y + r
#
#
# ans = list(map(sum_two_number, {1, 2, 3}, [1, 90], [4, 5]))
# print(ans)


# fsd = "a b c d e".split()
# print(fsd)

# pi = 'global variable'
#
#
# def outer():
#     pi = 'outer/enclose variable'
#
#     def inner():
#         nonlocal pi
#         # global pi
#         pi = 'inner/local variable'
#     print(pi)
#     inner()
#
#
# outer()
# print(pi)

'''
Создайте класс BlogPost с атрибутами user_name, text, number_of_likes. 
Создайте два объекта этого класса. 
После создания измените атрибут number_of_likes одного из объектов. 
Распечатайте атрибут number_of_likes каждого из объектов
'''

# class BlogPost:
#     def __init__(self, user_name, text, number_of_like):
#         self.user_name = user_name
#         self.text = text
#         self.number_of_like = number_of_like
#
#
# nature = BlogPost(user_name='STB',
#                   text='''
#                   the sum was shining bring
#                   ''',
#                   number_of_like=5690)
#
# ocean = BlogPost(user_name='BlueDeep',
#                  text='''
#                   Waves were huge
#                   ''',
#                  number_of_like=99)
#
# ocean.number_of_like = 24555
# print(nature.number_of_like,
#       ocean.number_of_like)

'''
Создайте класс BankAccount с атрибутами client_id, client_first_name, client_last_name, balance и методами add() и withdraw(),
при помощи которых можно пополнять счет и выводить средства со счета соответственно. 
Атрибут balance должен инициализироваться по умолчанию значением 0.0 и меняться при срабатывании методов add() и withdraw().
'''

# class BankAccount:
#     def __init__(self, client_first_name, client_last_name):
#         self.client_id = int(0107.0)
#         self.client_first_name = client_first_name
#         self.client_last_name = client_last_name
#         self.balance = 0.0

# def add(self, client_first_name, client_last_name, amount_add):
#     self.balance += float(amount_add)
#     self.client_first_name += client_first_name
#     self.client_last_name += client_last_name
#     print(f'Dear {client_first_name} {client_last_name}, you\'ve just added {amount_add}$, now you have {self.balance}% on your bank account')

#     def add(self, amount_add):
#         self.balance += float(amount_add)
#         print(
#             f'Dear {self.client_first_name} {self.client_last_name}, you\'ve just added {amount_add}$, now you have {self.balance}% on your bank account')
#
#     def withdraw(self, amount_withdraw):
#         self.balance -= float(amount_withdraw)
#         print(f'You\'ve just withdraw {amount_withdraw}$, now you have {self.balance}$ on your bank account')
#
#
# bank_account = BankAccount('Oleksii', 'Petlytskyi')

# bank_account.add(input('Please, enter your name: \n'),
#                  input('Please, enter your surname: \n'),
#                  input('Please, enter the amount of $ you want to deposit: \n'))

# bank_account.add(input('Please, enter the amount of $ you want to deposit: \n'))

# bank_account.withdraw(input('Please, enter the amount of $ you want to withdraw: \n'))

# Polymorphism
# class Dog:
#     def __init__(self, name):
#         self.name = name
#
#     def speak(self):
#         print(self.name + 'is saying Woof')
#
#
# class Cat:
#     def __init__(self, name):
#         self.name = name
#
#     def speak(self):
#         print(self.name + 'is saying meov')
#
#
# spike = Dog('Spike')
# tom = Cat('Tom')
#
# animals = [spike, tom]
# [pet.speak() for pet in animals]


'''
Создайте класс GameCharacter с атрибутами name, health, level и методом speak(), 
который выводит на печать 'Hi, my name is (значение атрибута name)'.
Создайте класс Villain, наследник класса GameCharacter с теми же атрибутами, методом speak(), 
который выводит на печать 'Hi, my name is (значение атрибута name) and I will kill you', методом kill(), 
который принимает в качестве параметра объект класса GameCharacter, присваивает атрибуту health этого объекта значение 0 и 
выводит на печать 'Bang-bang, now you're dead'
'''

# class GameCharacters:
#     def __init__(self, name, health, level):
#         self.name = name
#         self.level = level
#         self.health = health
#
#     def speak(self):
#         print(f'Hi, my name is {self.name} \n health: {self.health} \n level: {self.level}')
#
#
# class Villain(GameCharacters):
#
#     def speak(self):
#         print(f'Hi, my name is {self.name} and I will kill you')
#
#     def kill(self, opponent: GameCharacters):
#         opponent.health = 0
#         print('Bang-Bang, you are dead')
#
#
# tom = GameCharacters('Tom', 100, 99)
# tom.speak()
#
#
# den = Villain(name='Den', health=50, level=100)
# print(Villain.mro())  # .__mro__, .mro() method resolution order - узнать иерархия в наследовании
# den.speak()
# den.kill(tom)
# print(f"{tom.name}'s health: {tom.health}")


#
#
# class Animal:
#     def __init__(self, name):
#         self.name = name
#
#     def voice(self):
#         print(self.name)
#
#
# dog = Animal("Santa")
# dog.voice()
#
#
# # print(pet_voice())

'''
Есть список a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89].
Выведите все элементы, которые меньше 5.
'''
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# b = [c for c in a if c < 5]
# print(list(b))


# # Нужно вернуть список, который состоит из элементов, общих для этих двух списков.
#
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 9, 89]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#
# # list comprehension
# c = [i for i in a if i in b]
# print(list(c))
#
# filter/lambda
# result = list(filter(lambda elem: elem in b, a))
# print(result)

#
# dict_a = {1: 10, 2: 20}
# dict_b = {3: 30, 4: 40}
# dict_c = {5: 50, 6: 60}
#
# dict_q = {**dict_a, **dict_b, **dict_c}
# print(dict_q)
#
# dict_h = dict_a | dict_b | dict_c
# print(dict_h)
#
#
# dict_t = {}
# for i in (dict_a, dict_b, dict_c):
#     dict_t.update(i)
# print(dict_t)
#
# dict_d = []
# for a in dict_a.items():
#     dict_d.append(a)
#     for b in dict_b.items():
#         dict_d.append(b)
#         for c in dict_c.items():
#             dict_d.append(c)
#
# print(dict_d)

# Отсортируйте словарь по значению в порядке возрастания и убывания.

import operator

# dct = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
# list_items = dct.items()
# test = sorted(list_items, key=lambda x: x[1])
# new_dict = dict(test)
# print(new_dict)
# reslt = sorted(dct.items(), key=lambda x: dct[x[0]])
# print(reslt)

# new_dct = {k: dct[k] for k in dct}
# print(new_dct)


'''
Создайте класс Chain с атрибутом number_of_items.
Создайте два специальных метода в этом классе.
Первый должен при вызове встроенной функции print() для 
объекта этого класса выводить 'Chain with (значение number_of_items) items'
Второй должен при вызове встроенной функции len() для 
объекта этого класса возвращать значение number_of_items этого объекта
'''


# class Chain:
#     def __init__(self, number_of_items):
#         self.number_of_items = number_of_items
#
#     def __str__(self):
#         return f'Chain with {self.number_of_items} items'
#
#     def __len__(self):
#         return self.number_of_items
#
#
# chain = Chain(29)
# print(chain)
# print(len(chain))
# print(len([1, 2, 3]))

'''
Импортируйте встроенный модуль math. Вычислите при помощи функций этого модуля:
1. Корень квадратный из числа 123456789
2. Факториал числа 987
3. 876 в степени 54
'''

#
# import math
# #
# #
# math.sqrt(123456789)
# math.factorial(987)
# print(math.pow(876, 54))
# print(876 ** 54)


# # Пример итератора (список)
# my_list = [1, 2, 3]
# iter_obj = iter(my_list)
# for i in range(3):
#     print(next(iter_obj))
# # Вывод: 1

# # Пример генератора (генераторное выражение)
# gen = (x * 2 for x in range(5+1))
# for num in gen:
#     print(num)


# Пример генератора (функция с yield)
def my_generator():
    yield 1
    yield 2
    yield 3


gen_obj = my_generator()
print(next(gen_obj))  # Вывод: 1
print(next(gen_obj))  # Вывод: 1
print(next(gen_obj))  # Вывод: 1
