cars = ['lada', 'vw', 'skoda', 'cadillac', 'ford', 'toyota', 'fiat', 'tavria']
for x in cars:
    print(x.upper())

# to create a list of numbers from 0 to 5 and then to multiply by two every number in the list and print it
my_list = list(range(0, 15+1))
print(my_list)
for x in my_list:
    x = x * 2
    print(x)

# to print list in revers view
my_list.sort(reverse=True)
print(my_list)

# to print the max/min and sun
print("Max number is " + str(max(my_list)))
print("Max number is " + str(min(my_list)))
print("Max number is " + str(sum(my_list)))

# to print a half of the list
cars = ['lada', 'vw', 'skoda', 'cadillac', 'ford', 'toyota', 'fiat', 'tavria']
my_cars = cars[1:3]
my_cars = cars[:4]
print(my_cars)
my_cars = cars[-4:-1]
print(my_cars)

# to copy list
cars = ['lada', 'vw', 'skoda', 'cadillac', 'ford', 'toyota', 'fiat', 'tavria']
new_cars = cars[:]
print(new_cars)
