x = 0

while True:
    print(x + 1)
    x = x + 1
    if x == 50:
        break
# ----------------от до 5----------------------
for x in range(0, 5):
    print(x)
# ----------------start, end , step----------------------
for x in range(0, 50, 2):
    print(x)

for x in range(0, 50, 2):
    print("Number x = " + str(x))
    if x == 40:
        break
print("------EOF------")