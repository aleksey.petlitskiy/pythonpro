# Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence,
# which is the number of times you must multiply the digits in num until you reach a single digit.

# persistence(39) => 3  # Because 3*9 = 27, 2*7 = 14, 1*4=4# and 4 has only one digit.


# def persistence(n):
#     n = list(str(n))
#     b = int(n[0]) * int(n[1])
#
#     b = list(str(b))
#     c = int(b[0]) * int(b[1])
#     return c
#
#
# print(persistence(99))


def persistence(numbers):
    while len(str(numbers)) > 1:
        acc = 1
        for digit in str(numbers):
            acc *= int(digit)
        numbers = acc
    return numbers


print(persistence(999))




